(function () {
    'use strict';
    angular
        .module('App')
    .controller('Users.Controller', UsersController);
    
    function UsersController($scope, $http, $log) {

        $http.get('/api/Users')
        .success(function (result) {
            $scope.users = result;
            $log.info(result);
        })
        .error(function (data, status) {
            $log.error(data);
        });
    }
    
})();