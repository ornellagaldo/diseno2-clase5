﻿using Ecommerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Ecommerce.WebApi.Controllers
{
    public class OrdersController : ApiController
    {

        public IEnumerable<Order> GetAllOrders()
        {
            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                return ctx.Orders.Include("Buyer").Include("OrderLines").ToArray();
            }
        }

        public IHttpActionResult GetOrder(int id)
        {

            using (var ctx = new Ecommerce.DataAccess.EcommerceContext())
            {
                var order = ctx.Orders.Include("Buyer").FirstOrDefault((u) => u.OrderID == id);
                if (order == null)
                {
                    return NotFound();
                }
                return Ok(order);
            }
            
        }
    }
}